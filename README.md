# Installation guide #

## dependencies ##
- **Apache**: 2.4.x
    - **rewrite_module**
	- **mod_proxy**
- **NodeJs**>= 8.0.0
- **NPM**>= 3.0
- **adonisjs/cli**>= 4.0
- **supervisord**>=3.3.1 

## installation ##

- Configure Apache virtual host with a proxy reverse to **http://localthos:3333** for the directory PATH_TO_PROJECT_IN_SERVER.
- Copy project file to PATH_TO_PROJECT_IN_SERVER

```bash
cd PATH_TO_PROJECT_IN_SERVER
npm install
cp .env.example .env
adonis serve --dev
```

- Set the FACEBOOK & INSTAGRAM data into the .env file:
	- FACEBOOK_CLIENT_ID
	- FACEBOOK_CLIENT_SECRET
	
	- INSTAGRAM_CLIENT_ID
	- INSTAGRAM_CLIENT_SECRET
	- INSTAGRAM_CODE

- Optionally you can setup a script in supervisor conf.d in order to run the nodejs server.



## technology stack ##
- **AdonisJs** 4.0
- **Node.js**
- **Twitter Bootstrap** 4.0
- **jQuery**


# How To:

This repository is a pre-configured project structure taken from AdonisJS. This project is about to get the lastest Instagram Post with a given location.

![captura de pantalla de 2018-02-03 12-50-38](https://user-images.githubusercontent.com/5767551/35768843-dd9399b8-08e0-11e8-8bc4-9254813510f1.png)


## Input data

Insert the location you want to search.

![captura de pantalla de 2018-02-03 12-52-17](https://user-images.githubusercontent.com/5767551/35768864-15f5ff6c-08e1-11e8-863f-d7baf9bdae61.png)
