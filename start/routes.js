'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Route = use('Route');

Route.on('/').render('welcome');
Route.on('/privacy').render('privacy_policy');
Route.on('/tos').render('terms');

Route.get('login/facebook', 'LoginController.redirect');
Route.get('authenticated/facebook', 'LoginController.callback');

Route.get('logout', 'LoginController.logout');

Route
.group(() => {
	Route.post('updateToken', 'ApiController.getFacebookToken');
	Route.get('getFacebookLocations', 'ApiController.getFacebookLocations');
	Route.get('getFacebookPosts', 'ApiController.getFacebookPosts').middleware(['auth']);
})
.prefix('api/v1');

