
$(document).ready(function(){

    const tittle = "Welcome to Pinnd";
    const search_text = "Let's Explore ";
    const lead = "Explore the world around you and start planning your next adventure!";


    $.ajax({ 
      url: '/api/v1/updateToken',
      type: 'POST',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
      }
    });


    var bestPictures = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      prefetch: '../data/locations/facebook_location.json',
      remote: {
      url: '../api/v1/getFacebookLocations?q=%QUERY',
      wildcard: '%QUERY'
      }
    });

    let typeahead = $('.typeahead').typeahead(null, {
      name: 'facebook-location',
      display: 'name',
      source: bestPictures,
      templates: {
        empty: [
          '<div class="empty-message">',
            'unable to find any location that match the current query',
          '</div>'
        ].join('\n'),
        pending: [
          '<div class="empty-message">',
            'Looking for results...',
          '</div>'
        ].join('\n'),
        suggestion: function(data) {
          if (data.location.city !== undefined && data.location.country !== undefined){
            return '<div><strong>' + data.name + '</strong> – ' + data.location.city + ', '+ data.location.country +'</div>';
          }else{
            return '<div><strong>' + data.name + '</strong></div>';
          }
        }
      }
    });

    $('.typeahead').bind('typeahead:change', function(ev, suggestion) {
      if (suggestion == ''){
        $('.container').find('.ig_post').not('.card-template').remove();
        $('#search').removeClass('show-results');
        $('#search').addClass('search');
        $('.banner-tittle').html(tittle);
        $('.banner-search').addClass('displaynone');
        $('.banner-lead').html(lead);
      }
    });

    $('.typeahead').bind('typeahead:select', function(ev, suggestion) {

      if (!logged){
        $('#login_popup').modal('show');
        $('#login_popup').on('shown.bs.modal', function (e) {
          $("#fb_login").click(function(){
            window.location.href = "./login/facebook?fb_location=" + suggestion.name + '&fb_location_id=' + suggestion.id;
          });
        });
        $('#login_popup').on('hide.bs.modal', function (e) {
          $("#fb_login").off();
        });
        return false;
      }


      /**
       * Banner texts
       */
      $('.banner-tittle').html("Here you go!");
      $('.banner-search').html(search_text + suggestion.name);
      $('.banner-search').removeClass('displaynone');
      $('.banner-lead').html("Otherwise... let's keep exploring.");



      $('.container').find('.ig_post').not('.card-template').remove();
      $('#search').removeClass('show-results');
      $('#search').addClass('search');
      if (!Object.keys(suggestion).length){
        return;
      }
      if (suggestion.id === undefined){
       return;       
      }

      $.ajax({ 
        url: '/api/v1/getFacebookPosts?fb_location=' + suggestion.id,
        type: 'GET',
        success: function(response){
          if (response.length > 0){
            $('#search').removeClass('search');
            $('#search').addClass('show-results');
          }
          $.each(response, function( index, post ) {
            var element = $('.container').find('.card-template').clone();

            element.find('a').attr("href", post.link);
            element.find('.card img').attr("src", post.images.standard_resolution.url);
            element.find('.card .card-body h4').html('@' + post.caption.from.username);
            element.find('.card .card-body p').html(post.caption.text);

            element.removeClass('displaynone');
            element.removeClass('card-template');
            element.insertBefore('.card-template');
          });

        },
        error: function(response){
          const error = JSON.parse(response.responseText)
          $('#search').removeClass('search');
          $('#search').addClass('show-results');
          var element = $('.container').find('.card-template').clone();
          element.find('.card .card-body h4').html("Error");
          element.find('.card .card-body p').html(error.error.message);


          element.removeClass('displaynone');
          element.removeClass('card-template');
          element.insertBefore('.card-template');
        }
      });

    });

    if ($('#old_fb_location_id').val() != '' && $("#inlineFormInputName2").val() != ''){

      /**
       * Banner texts
       */
      $('.banner-tittle').html("Here you go!");
      $('.banner-search').html(search_text + $("#inlineFormInputName2").val());
      $('.banner-search').removeClass('displaynone');
      $('.banner-lead').html("Otherwise... let's keep exploring.");



      $('.container').find('.ig_post').not('.card-template').remove();
      $('#search').removeClass('show-results');
      $('#search').addClass('search');

      $.ajax({ 
        url: '/api/v1/getFacebookPosts?fb_location=' + $('#old_fb_location_id').val(),
        type: 'GET',
        success: function(response){
          if (response.length > 0){
            $('#search').removeClass('search');
            $('#search').addClass('show-results');
          }
          $.each(response, function( index, post ) {
            var element = $('.container').find('.card-template').clone();

            element.find('a').attr("href", post.link);
            element.find('.card img').attr("src", post.images.standard_resolution.url);
            element.find('.card .card-body h4').html('@' + post.caption.from.username);
            element.find('.card .card-body p').html(post.caption.text);

            element.removeClass('displaynone');
            element.removeClass('card-template');
            element.insertBefore('.card-template');
          });

        },
        error: function(response){
          const error = JSON.parse(response.responseText)
          $('#search').removeClass('search');
          $('#search').addClass('show-results');
          var element = $('.container').find('.card-template').clone();
          element.find('.card .card-body h4').html("Error");
          element.find('.card .card-body p').html(error.error.message);


          element.removeClass('displaynone');
          element.removeClass('card-template');
          element.insertBefore('.card-template');
        }
      });
    }

});