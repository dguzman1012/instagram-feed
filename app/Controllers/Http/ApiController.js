'use strict'

const got = use('got')
const Env = use('Env')

class ApiController {

	async getFacebookToken({request, response, session}){
		const url = 'https://graph.facebook.com/oauth/access_token?client_id='+ Env.get('FB_CLIENT_ID') +'&client_secret='+ Env.get('FB_CLIENT_SECRET') +'&grant_type=client_credentials';
		const res = await got(url)
		const facebook_token = JSON.parse(res.body).access_token
		session.put('facebook_token', facebook_token)

		response.json({
			success: true
		})

	}

	async getFacebookLocations({request, response, session}){
		let query = request.get()
		if (!Object.keys(query).length){
			response.json({success:false, data:{}})
			return;
		}

		if (query.q === undefined){
			response.json({success:false, data:{}})
			return;				
		}

		const url = "https://graph.facebook.com/v2.11/search?type=place&q="+query.q+"&fields=name,checkins,picture,location,rating_count&access_token=" + session.get("facebook_token");
		const res = await got(url)
		const locations = JSON.parse(res.body)
		if (locations.data){
			response.json(locations.data)
		}else{
			response.json({
				success: false
			})
		}
	}

	async getFacebookPosts({request, response, auth}){
		const AccessToken = auth.user.access_token;
		let query = request.get()
		if (!Object.keys(query).length){
			response.json({success:false, data:{}})
			return;
		}

		if (query.fb_location === undefined){
			response.json({success:false, data:{}})
			return;				
		}

		const facebook_location = query.fb_location;

		/**
		 * Getting the FB page feed
		 */
		
		const url = "https://graph.facebook.com/v3.0/"+facebook_location+"?fields=page{feed{full_picture,from, caption,type}}&access_token=" + AccessToken;

		try {
	        const fb_posts_data = await got(url)
			const posts = JSON.parse(fb_posts_data.body).page.feed.data.slice(0, 9)
			if (posts.length >=0){
				response.json(posts)
			}else{
				response.json({
					success: false
				})
			}
	    } catch (error) {
	        response.status(400).json(error.response.body);
	    }

		
	}

}

module.exports = ApiController
