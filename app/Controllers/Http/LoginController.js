'use strict'

const User = use('App/Models/User')

class LoginController {

	async redirect ({ ally, response, request, session }) {

		const fb_location = request.input('fb_location', null);
		const fb_location_id = request.input('fb_location_id', null);
		if (fb_location){
			session.put('fb_location', fb_location)
		}

		if (fb_location_id){
			session.put('fb_location_id', fb_location_id)	
		}

		await ally.driver('facebook').redirect()
	}

	async callback ({ ally, auth, response, session }) {
		try{
			const igUser = await ally.driver('facebook').getUser()

			// user details to be saved
			const userDetails = {
				username: igUser.getNickname(),
				email: igUser.getEmail(),
				access_token: igUser.getAccessToken(),
				login_source: 'facebook'
			}

			// search for existing user
			const whereClause = {
				username: igUser.getNickname()
			}

			const user = await User.findOrCreate(whereClause, userDetails)
			user.access_token = userDetails.access_token
			await user.save()


			let fb_location = session.pull('fb_location')
			if (fb_location){
				session.flash({ fb_location: fb_location })	
			}

			let fb_location_id = session.pull('fb_location_id')
			if (fb_location_id){
				session.flash({ fb_location_id: fb_location_id })	
			}
			await auth.login(user)

			response.redirect('/')
		}catch(error){
			response.redirect('/')
		}
		
	}

	async logout ({ auth, response}){
		try{
			await auth.logout()
			response.redirect('/')
		}catch (error){
			response.redirect('/')
		}
	}
}

module.exports = LoginController
